#!/usr/bin/env python3

import grass.script as gscript


def main():
    i=0

    while i<10:

        gscript.run_command('r.gwflow', 
            top="top_unconf",
            bottom="null",
            phead="phead_"+str(i),
            status="status",
            q="well",
            hc_x="hydcond",
            hc_y="hydcond",
            s="poros",
            output="phead_"+str(i+1),
            type="unconfined",
            dt="86400",
            error="0.000000000000001",
            maxit="100000000000",
            overwrite=True)

        i=i+1


if __name__ == '__main__':
    main()
